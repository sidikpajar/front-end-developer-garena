import axios from 'axios';

import { 
  FETCH_UNIVERSITY_REQUEST,
  FETCH_UNIVERSITY_SUCCESS,
  FETCH_UNIVERSITY_FAILURE
 } from './universityTypes';

export const fetchUniversityRequest = () => {
  return {
    type: FETCH_UNIVERSITY_REQUEST
  }
}

export const fetchUniversitySuccess = university => {
  return {
    type: FETCH_UNIVERSITY_SUCCESS,
    payload: university,
  }
}

export const fetchUniversityFailure = error => {
  return {
    type: FETCH_UNIVERSITY_FAILURE,
    payload: error
  }
}

export const fetchUniversity = () => {
  
  return (dispatch) => {
    dispatch(fetchUniversityRequest)
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_UNIVERSITY}`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {
    
      const university = response.data
      dispatch(fetchUniversitySuccess(university))
    })
    .catch(errors => {
      dispatch(fetchUniversityFailure(errors))
    })
  }
}