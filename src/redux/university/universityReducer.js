import { 
  FETCH_UNIVERSITY_REQUEST,
  FETCH_UNIVERSITY_SUCCESS,
  FETCH_UNIVERSITY_FAILURE
 } from './universityTypes';

const initialState = {
  loading: false,
  university: {},
  error: ''
}

const universityReducer = (state = initialState, action) => {
  switch(action.type){
    case FETCH_UNIVERSITY_REQUEST : return {
      ...state,
      loading: true
    }
    case FETCH_UNIVERSITY_SUCCESS : return {
      loading: false,
      university: action.payload,
      error: ''
    }
    case FETCH_UNIVERSITY_FAILURE : return {
      loading: false,
      university: [],
      error: action.payload
    }
    default: return state
  }
}

export default universityReducer