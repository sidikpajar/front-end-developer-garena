import { combineReducers } from 'redux'
import contentReducer from './content/contentReducer'
import universityReducer from './university/universityReducer'
import facultyReducer from './faculty/facultyReducer'
import testimonialsReducer from './testimonials/testimonialsReducer'

const rootReducer = combineReducers({
  university: universityReducer,
  content: contentReducer,
  faculty: facultyReducer,
  testimonials: testimonialsReducer,
})

export default rootReducer