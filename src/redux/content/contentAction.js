import axios from 'axios';

import { 
  FETCH_CONTENT_REQUEST,
  FETCH_CONTENT_SUCCESS,
  FETCH_CONTENT_FAILURE
 } from './contentTypes';

export const fetchContentRequest = () => {
  return {
    type: FETCH_CONTENT_REQUEST
  }
}

export const fetchContentSuccess = content => {
  return {
    type: FETCH_CONTENT_SUCCESS,
    payload: content,
  }
}

export const fetchContentFailure = error => {
  return {
    type: FETCH_CONTENT_FAILURE,
    payload: error
  }
}

export const fetchContent = () => {
  return (dispatch) => {
    dispatch(fetchContentRequest)
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_CONTENT}`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {

      const content = response.data
      dispatch(fetchContentSuccess(content))
    })
    .catch(errors => {
      dispatch(fetchContentFailure(errors))
    })
  }
}