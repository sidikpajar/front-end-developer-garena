import { 
  FETCH_CONTENT_REQUEST,
  FETCH_CONTENT_SUCCESS,
  FETCH_CONTENT_FAILURE
 } from './contentTypes';

const initialState = {
  loading: false,
  content: [],
  error: ''
}

const contentReducer = (state = initialState, action) => {
  switch(action.type){
    case FETCH_CONTENT_REQUEST : return {
      ...state,
      loading: true
    }
    case FETCH_CONTENT_SUCCESS : return {
      loading: false,
      content: action.payload,
      error: ''
    }
    case FETCH_CONTENT_FAILURE : return {
      loading: false,
      content: [],
      error: action.payload
    }
    default: return state
  }
}

export default contentReducer