import axios from 'axios';

import { 
  FETCH_FACULTY_REQUEST,
  FETCH_FACULTY_SUCCESS,
  FETCH_FACULTY_FAILURE
 } from './facultyTypes';

export const fetchFacultyRequest = () => {
  return {
    type: FETCH_FACULTY_REQUEST
  }
}

export const fetchFacultySuccess = faculty => {
  return {
    type: FETCH_FACULTY_SUCCESS,
    payload: faculty,
  }
}

export const fetchFacultyFailure = error => {
  return {
    type: FETCH_FACULTY_FAILURE,
    payload: error
  }
}

export const fetchFaculty = () => {
  
  return (dispatch) => {
    dispatch(fetchFacultyRequest)
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_FACULTY}`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {
    
      const faculty = response.data
      dispatch(fetchFacultySuccess(faculty))
    })
    .catch(errors => {
      dispatch(fetchFacultyFailure(errors))
    })
  }
}