import { 
  FETCH_FACULTY_REQUEST,
  FETCH_FACULTY_SUCCESS,
  FETCH_FACULTY_FAILURE
 } from './facultyTypes';

const initialState = {
  loading: false,
  faculty: {},
  error: ''
}

const facultyReducer = (state = initialState, action) => {
  switch(action.type){
    case FETCH_FACULTY_REQUEST : return {
      ...state,
      loading: true
    }
    case FETCH_FACULTY_SUCCESS : return {
      loading: false,
      faculty: action.payload,
      error: ''
    }
    case FETCH_FACULTY_FAILURE : return {
      loading: false,
      faculty: [],
      error: action.payload
    }
    default: return state
  }
}

export default facultyReducer