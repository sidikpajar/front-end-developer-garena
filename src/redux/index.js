export * from './faculty/facultyAction'
export * from './university/universityAction'
export * from './content/contentAction'
export * from './testimonials/testimonialsAction'