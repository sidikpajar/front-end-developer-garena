import axios from 'axios';

import { 
  FETCH_TESTIMONIALS_REQUEST,
  FETCH_TESTIMONIALS_SUCCESS,
  FETCH_TESTIMONIALS_FAILURE
 } from './testimonialsTypes';

export const fetchTestimonialsRequest = () => {
  return {
    type: FETCH_TESTIMONIALS_REQUEST
  }
}

export const fetchTestimonialsSuccess = testimonials => {
  return {
    type: FETCH_TESTIMONIALS_SUCCESS,
    payload: testimonials,
  }
}

export const fetchTestimonialsFailure = error => {
  return {
    type: FETCH_TESTIMONIALS_FAILURE,
    payload: error
  }
}

export const fetchTestimonials = () => {
  
  return (dispatch) => {
    dispatch(fetchTestimonialsRequest)
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_TESTIMONIALS}`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {
    
      const testimonials = response.data
      dispatch(fetchTestimonialsSuccess(testimonials))
    })
    .catch(errors => {
      dispatch(fetchTestimonialsFailure(errors))
    })
  }
}