import { 
  FETCH_TESTIMONIALS_REQUEST,
  FETCH_TESTIMONIALS_SUCCESS,
  FETCH_TESTIMONIALS_FAILURE
 } from './testimonialsTypes';

const initialState = {
  loading: false,
  testimonials: {},
  error: ''
}

const testimonialsReducer = (state = initialState, action) => {
  switch(action.type){
    case FETCH_TESTIMONIALS_REQUEST : return {
      ...state,
      loading: true
    }
    case FETCH_TESTIMONIALS_SUCCESS : return {
      loading: false,
      testimonials: action.payload,
      error: ''
    }
    case FETCH_TESTIMONIALS_FAILURE : return {
      loading: false,
      testimonials: [],
      error: action.payload
    }
    default: return state
  }
}

export default testimonialsReducer