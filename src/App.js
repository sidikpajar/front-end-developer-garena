import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import './styles/index.scss';
import Menu from './pages/molecules/menu';
import Header from './pages/molecules/header';
import Undergraduate from './pages/molecules/undergraduate';
import Partner from './pages/molecules/partner';
import Timeline from './pages/molecules/timeline';
import Testimonials from './pages/molecules/testimonials';
function App() {
  return (
    <Provider store={store}>
        <Menu />
        <Header />
        <Undergraduate />
        <Partner />
        <Timeline />
        <Testimonials />
    </Provider>
  );
}

export default App;
