import React from 'react';


export default function TimelineSection({title, value, iconTimeline}) {
  return (
    <div className="text-center">
      <div className="timeline-title">{title}</div>
      <img
        src={iconTimeline}
        width={200}
        alt={title}
      />
      <div className="timeline-value">{value}</div>
    </div>
      
  )
}
