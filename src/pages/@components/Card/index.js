import React from 'react';


export default function Card({title, data, iconProgram}) {
  return (
    <div className="card">
      <img
        src={iconProgram}
        width={500}
        className="iconProgram"
        alt={title}
      />
    <div className="undergraduate-title-program">{title}</div>
      {data}
    </div>
  )
}
