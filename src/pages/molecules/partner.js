import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchUniversity, fetchFaculty } from '../../redux'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';


function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
       const key = keyGetter(item);
       const collection = map.get(key);
       if (!collection) {
           map.set(key, [item]);
       } else {
           collection.push(item);
       }
  });
  return map;
}

class partner extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data:[
        ],
    }
  }
  

  async componentDidMount(){
    await this.props.fetchUniversity()
    await this.props.fetchFaculty()

  }

  render() {
    const {universityData} = this.props;
    const {facultyData} = this.props;
    let grouped = [];
    if(facultyData !== undefined){
      grouped = groupBy(facultyData, faculty => faculty.univ_id);
    } else {
      grouped = groupBy([], faculty => faculty.univ_id);
    }
    return (
      <div>
         <div className="col-12 title">Partner Universities</div>
      <Carousel>
        {
          universityData ? universityData.map(row =>  
          <div className="carousel-custom">
            <div className="row">
              <div className="col-12 univ-name">{row.univ_name}</div>
            </div>
            <div className="row">
              <div className="col-4 logo-partner" >
                <img style={{width:300}} src={row.univ_logo} alt={row.univ_name}/>
              </div>
              <div className="col-8 logo-partner">
                <div className="partner">
                  <ul className="columns">
                    {
                      grouped.get(row.id) ? grouped.get(row.id).map(rows => 
                      <li key={rows.id}>{rows.name}</li>) : <li>tidak ada data</li>
                    }
                  </ul>
                </div>
              </div>
            </div>
          </div>
          ) : ''
        }
      </Carousel>
      </div>
    )
  }
}



const MapStateToProps = (state) => {
  return {
    universityData: state.university.university.data,
    facultyData: state.faculty.faculty.data
  }
}

const MapDispatchToProps = (dispatch) => {
  return {
    fetchUniversity: () => dispatch(fetchUniversity()),
    fetchFaculty: () => dispatch(fetchFaculty()),
  }
}


export default connect(MapStateToProps, MapDispatchToProps)(partner)

