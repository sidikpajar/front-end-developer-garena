import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchContent } from '../../redux'
import { TimelineSection } from '../@components'
import Timeline1 from '../../assets/Slicing/Timeline-1.png';
import Timeline2 from '../../assets/Slicing/Timeline-2.png';
import Timeline3 from '../../assets/Slicing/Timeline-3.png';
import Timeline4 from '../../assets/Slicing/Timeline-4.png';

class timeline extends Component {


  async componentDidMount(){
    await this.props.fetchContent()
  }

  render() {
    const {contentData } = this.props;
    const date_announce = contentData ? contentData[0].date_announce : '';
    const date_esay_cvscreen = contentData ? contentData[0].date_esay_cvscreen : '';
    const date_interview = contentData ? contentData[0].date_interview : '';
    const date_regis = contentData ? contentData[0].date_regis : '';
    return (
      <div className="background-grey timelince-section">
        <div className="container">
          <h2 className="title">Timeline</h2>
          <div className="row">
            <div className="col-3">
              <TimelineSection 
                iconTimeline={Timeline1}
                value={date_announce}
                title="Registration"
              />
            </div>
            <div className="col-3">
              <TimelineSection 
                iconTimeline={Timeline2}
                value={date_esay_cvscreen}
                title="Essay & CV Screening"
              />
            </div>
            <div className="col-3">
              <TimelineSection 
                iconTimeline={Timeline3}
                value={date_interview}
                title="On-campus Interview"
              />
            </div>
            <div className="col-3">
              <TimelineSection 
                iconTimeline={Timeline4}
                value={date_regis}
                title="Announcement of Selected Scholars"
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const MapStateToProps = (state) => {
  return {
    contentData: state.content.content.data,
  }
}

const MapDispatchToProps = (dispatch) => {
  return {
    fetchContent: () => dispatch(fetchContent()),
  }
}

export default connect(MapStateToProps, MapDispatchToProps)(timeline)
