import React, { Component } from 'react'
import IconGraduate from '../../assets/Slicing/icon-graduate.png';

class header extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (
      <div className="bgHead">
        <div className="container header">
          <div className="row">
            <div className="col-6">
              <h1 className="title-white">Congratulations Sea Scholarship 2019 Awardees!</h1>
              <p className="title-description">The Sea Scholarship Commitee is proud to announce the 2019 Academic Year Sea Scholarship recipients. Final award email notifications were sent on October 17,2019.</p>
            </div>
            <div className="col-6">
                <img
                  src={IconGraduate}
                  width={500}
                  className="image-header"
                  alt="Sea-Undergraduate-Logo"
                />
            </div>
          </div>
        </div>
      </div> 
    )
  }
}


export default header;
