import React, { Component } from 'react'
import Logo from '../../assets/Slicing/Sea-Undergraduate-Logo.png';

class menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6 image-responsive">
            <img
              src={Logo}
              className="logo"
              alt="Sea-Undergraduate-Logo"
            />
          </div>
          <div className="col-6">
            <ul className="menu">
              <li><a href="#">Home</a></li>
              <li><a href="#">Program</a></li>
              <li><a href="#">Apply Here</a></li>
              <li><a href="#">FAQ</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}


export default menu;
