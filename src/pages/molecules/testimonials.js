import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchTestimonials } from '../../redux'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';


class testimonials extends Component {

  async componentDidMount(){
    await this.props.fetchTestimonials()
  }

  render() {
    const {testimonialsData} = this.props;
    console.log(testimonialsData);
    return (
      <div>
        <div className="col-12 title">Testimonials</div>
        <Carousel>
        {
          testimonialsData ? testimonialsData.map(row =>  
          <div className="container">
            <div className="carousel-custom">
              <div className="row">
                <div className="col-4 logo-partner" >
                  <img style={{width:300}} src={row.image} alt={row.name}/>
                </div>
                <div className="col-8">
                    <p className="story">{row.story}</p>
                    <p className="name">{row.name}</p>
                </div>
              </div>
            </div>
          </div>
          
          ) : ''
        }
      </Carousel>
      </div>
    )
  }
}



const MapStateToProps = (state) => {
  return {
    testimonialsData: state.testimonials.testimonials.data,
  }
}

const MapDispatchToProps = (dispatch) => {
  return {
    fetchTestimonials: () => dispatch(fetchTestimonials()),
  }
}


export default connect(MapStateToProps, MapDispatchToProps)(testimonials)

