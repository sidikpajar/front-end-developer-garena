import React, { Component } from 'react'
import freshmanIcon from '../../assets/Slicing/icon-freshmen.png';
import seniorIcon from '../../assets/Slicing/icon-senior.png';
import { connect } from 'react-redux'
import { fetchContent } from '../../redux'
import htmr from 'htmr';
import { Card } from '../@components';


class undergraduate extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  async componentDidMount(){
    await this.props.fetchContent()
  }

  
  render() {
    const {contentData } = this.props;
    const freshman = contentData ? contentData[0].freshman_program : '';
    const program_content = contentData ? contentData[0].program_content : '';
    const senior_program = contentData ? contentData[0].senior_program : '';
    return (
      <div className="background-grey background-undergraduate">
        <div className="container undergraduate">
            <h2 className="title">Undergraduate Scholarship Program</h2>
            <p className="undergraduate-description">{program_content}</p>
            <div className="row">
              <div className="col-6">
                <Card 
                  iconProgram={freshmanIcon}
                  title="Sea Freshman Scholarship Program"
                  data={htmr(freshman)} 
                />
              </div>
              <div className="col-6">
                <Card 
                  iconProgram={seniorIcon}
                  title="Sea Senior Scholarship Program"
                  data={htmr(senior_program)} 
                />
              </div>
            </div>
        </div>
      </div>
      
    )
  }
}


const MapStateToProps = (state) => {
  return {
    contentData: state.content.content.data,
  }
}

const MapDispatchToProps = (dispatch) => {
  return {
    fetchContent: () => dispatch(fetchContent()),
  }
}

export default connect(MapStateToProps, MapDispatchToProps)(undergraduate)
